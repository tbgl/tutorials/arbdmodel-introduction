set beg 0
set skip 1
set end -1
# set end 1400

# mol new PORE.dx

set prefix trombone-ssb/output/run

set IDs [mol new trombone-ssb/run.psf]

set wd [pwd]

set naLength 1
source loadRigidBody.procs.tcl
dcd $prefix.dcd skip $skip beg $beg end $end


if [file exists $prefix.0.rb-traj] {
    set rbIDs [loadTrajectory $prefix.0.rb-traj $IDs $skip $beg $end]
} else {
    set rbIDs [loadTrajectory $prefix.rb-traj $IDs $skip $beg $end]
}   
# loadTrajectoryRbFrame $files [lindex $IDs 0] $skip $beg $endo

cd $wd

foreach ID $IDs {
    set sel [atomselect $ID all]
    set beta ""
    foreach r [$sel get resid] {
	lappend beta [expr $r%10 + double($r)/10] 
    }
    $sel set beta $beta

    set sel [atomselect $ID "name P"]
    $sel set radius 2.5
    set sel [atomselect $ID "name B"]
    $sel set radius 2

    mol color Name
    # mol selection all
    # mol selection "name P B"
    mol selection "name P"
    mol material AOChalky
    # mol representation Licorice 1.3
    mol representation Licorice 2.0
    mol modrep 0 $ID
}


# mol color Beta
mol color ColorID 15
# mol color SegName
# mol color Molecule
mol selection "protein"
mol material AOChalky
# mol material GlassBubble
# mol representation VDW 0.8
# mol representation QuickSurf

# mol representation VDW 2.000000 12.000000
# mol representation NewCartoon
mol representation QuickSurf 1.000000 0.500000 1.900000 1.000000 
foreach rbID $rbIDs { 
    # mol color ColorId $rbID
    mol modrep 0 $rbID
}
# mol off [lindex $rbIDs 12]

# color Display Background white
color Name P green
color Name B green2
color change rgb green2 0.7 0.9 0.7

color Segname APRO cyan2
color Segname BPRO blue
color Segname CPRO cyan3
color Segname DPRO blue3

# light 0 off
# display depthcue off
# display backgroundgradient off
display shadows on
display ambientocclusion on
display aoambient 0.6
display aodirect 0.6

set sysID [mol new replisome.psf]
mol addfile replisome.pdb

mol material AOChalky
mol representation NewCartoon
# mol color Segname
mol color ColorID 4
mol selection "segname V013 V019 V021 and x > 0"
mol modrep 0 $sysID

mol material GlassBubble
mol representation QuickSurf 1.5
mol addrep $sysID

mol material AOChalky
mol representation NewCartoon
mol color ColorID 10
mol selection "segname V001 V003 V005 V007 V009 V011 and resid > 260 and x > 0"
mol addrep $sysID

mol material GlassBubble
mol representation QuickSurf 1.5
mol addrep $sysID

mol material AOChalky
mol representation NewCartoon
mol color ColorID 11
mol selection "segname V009 V011 V027 V029 V031 V033 and resid <= 260 and x > 0"
mol addrep $sysID

mol material GlassBubble
mol representation QuickSurf 1.5
mol addrep $sysID

mol material AOChalky
mol representation VDW
mol color ColorID 7
mol selection nucleic
mol addrep $sysID

mol top $ID
