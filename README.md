# ARBDmodel

This tutorial introduces a Python3 package called [`arbdmodel`](https://gitlab.engr.illinois.edu/tbgl/tools/arbdmodel) that provides a convenient interface for building coarse-grained models of biological and nanotechnological systems that run on the [ARBD](http://bionano.physics.illinois.edu/arbd) simulation engine.
The tutorial shows you how to use existing models of ssDNA and disordered peptides, how to include confinement potentials, and how to run simulations that include rigid body systems.

## Required hardware and operating system

Please note that this tutorial is written for the Linux platform and x86 architecture.
Proceed at your own risk using other operating systems and architectures.
Currently, a CUDA-enabled GPU is required for running simulations using the ARBD engine.

## Required software

Make sure you have the [`arbdmodel`](https://gitlab.engr.illinois.edu/tbgl/tools/arbdmodel) package properly installed.
You will also need the `jupyter` package installed to your Python environment. 
Finally, you will need to install
[VMD](https://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=VMD)
and [ARBD](http://bionano.physics.illinois.edu/arbd).

If you have any questions or concerns, please contact Chris Maffeo at [cmaffeo2@illinois.edu](mailto:cmaffeo2@illinois.edu) or open an issue on the [gitlab site](https://gitlab.engr.illinois.edu/tbgl/tutorials/arbdmodel-introduction).
