{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic usage of the `arbdmodel` package\n",
    "\n",
    "We'll learn how to build simple models using the arbdmodel Python package that will run on the Atomic Resolution Brownian Dynamics (ARBD/arbd) simulation engine. ARBD requires a GPU and linux operating system.\n",
    "\n",
    "This tutorial assumes you have a working knowledge of Python, including its object-oriented features, and the numpy package.\n",
    "\n",
    "Also, a word of caution: at the time of writing ARBD will not prevent you from simulating with poorly selected runtime parameters (e.g. too large a timestep; or too large a decomposition period with too small a pairlist distance; or simply potentials that prescribe unreasonably large forces). Hence it is crucial to have a good understanding of the MD/BD algorithm if you are using it in production settings."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 1: Create particle types "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import ipdb # in case we need to debug, after a crash, run `ipdb.pm()`\n",
    "import numpy as np\n",
    "from arbdmodel import ParticleType, PointParticle, ArbdModel\n",
    "\n",
    "system_size = 620\n",
    "num_particles = 6400\n",
    "\n",
    "## diffusion of liquid argon: https://www.sciencedirect.com/science/article/pii/0375960171901290         \n",
    "## units \"60 * 3.41 AA * sqrt(119.5 k K / 40 dalton)\" \"AA**2/ns\"  \n",
    "argon = ParticleType(name=\"Ar\",\n",
    "                     mass=39.95, # dalton\n",
    "                     damping_coefficient=5000, # per ns                                                  \n",
    "                     custom_property=\"my_value\",\n",
    "                     epsilon=0.177,  # kcal_mol                                                          \n",
    "                     radius=3.345/2) # Angstroms\n",
    "\n",
    "print(argon)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 2: Build a system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Generate Nx3 array of random cartesian coordinates\n",
    "positions = system_size*(np.random.random([num_particles,3])-0.5)\n",
    "\n",
    "## Create a list of point particles located at those positions\n",
    "points = [PointParticle(type_=argon, name=\"Ar\", position=positions[i])\n",
    "                        for i in range(num_particles)]\n",
    "\n",
    "model = ArbdModel(points, \n",
    "                  dimensions=[system_size for i in range(3)], # Ångstroms\n",
    "                  timestep = 20e-6, # ns; can be specified below with engine instead\n",
    "                  )\n",
    "\n",
    "print(model)\n",
    "print(model.children[:2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 3: Describe the interactions between the particles \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from arbdmodel.interactions import LennardJones\n",
    "\n",
    "lj = LennardJones()\n",
    "model.add_nonbonded_interaction(lj)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 4: Run the simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from arbdmodel import ArbdEngine\n",
    "\n",
    "engine = ArbdEngine(output_period = 1e2, num_steps = 1e5)\n",
    "engine.simulate(model, output_name=\"1-lj\", directory='sims/1-argon',\n",
    "                num_steps=1e3, gpu=1\n",
    ")                               # simulation parameters here override those in constructor \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 5: Visualize the results\n",
    "\n",
    "We recommend using [VMD](https://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=VMD) for visualizing the simulations trajectories with:\n",
    "```bash\n",
    "vmd sims/1-argon/1-lj.psf sims/1-argon/output/1-lj.dcd\n",
    "```\n",
    "However, for convenience we have provided commands to use the nglview module, which provides browser-based visualization.\n",
    "\n",
    "note: you must enable certain jupyter extensions to use nglview as follows before running jupyter-notebook\n",
    "```bash\n",
    "jupyter-nbextension enable --py --sys-prefix widgetsnbextension\n",
    "jupyter-nbextension enable nglview --py --sys-prefix\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import MDAnalysis as mda\n",
    "import nglview as nv\n",
    "\n",
    "u = mda.Universe(\"sims/1-argon/1-lj.pdb\", \"sims/1-argon/output/1-lj.dcd\")\n",
    "w = nv.show_mdanalysis(u)\n",
    "w.clear_representations()\n",
    "w.add_ball_and_stick(\"all\",radius=10)\n",
    "w"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 6: Customize the interactions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from arbdmodel.interactions import AbstractPotential\n",
    "\n",
    "## Clear nonbonded interactions                                                                          \n",
    "model.nonbonded_interactions = []\n",
    "\n",
    "class BuckinghamPotential(AbstractPotential):\n",
    "    def potential(self, r, types):\n",
    "        ## https://royalsocietypublishing.org/doi/10.1098/rspa.1938.0173                                 \n",
    "        ## optionally could pull information from typeA, typeB                                           \n",
    "        typeA, typeB = types\n",
    "        return 143932.65 * ( 1.69 * np.exp( -r/0.273 ) - 102e-4 / r**6 )\n",
    "\n",
    "pot = BuckinghamPotential()\n",
    "model.add_nonbonded_interaction( pot )\n",
    "\n",
    "engine.simulate(model, output_name=\"2-buck\", directory='sims/1-argon',\n",
    "                num_steps = 1e3, gpu=1\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import MDAnalysis as mda\n",
    "import nglview as nv\n",
    "\n",
    "u = mda.Universe(\"sims/1-argon/2-buck.pdb\", \"sims/1-argon/output/2-buck.dcd\")\n",
    "w = nv.show_mdanalysis(u)\n",
    "w.clear_representations()\n",
    "w.add_ball_and_stick(\"all\",radius=10)\n",
    "w"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 7: Add bonds\n",
    "\n",
    "Argon is, of course, a noble gas and will not form bonds; this is a demonstration only, and not representative of a physical system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from arbdmodel.interactions import HarmonicBond\n",
    "bond = HarmonicBond(k=100,           # kcal/mol AA**2                                                      \n",
    "                    r0 = 3         # AA                                                                  \n",
    ")\n",
    "\n",
    "## Go through every other index                                                                          \n",
    "for i in range(0,len(model.children),2):\n",
    "    j = i+1\n",
    "    model.add_bond( model.children[i],\n",
    "                    model.children[j],\n",
    "                    bond )\n",
    "\n",
    "print(\"numbonds:\", len(model.bonds))\n",
    "\n",
    "engine.simulate(model, output_name=\"3-bonds\", directory='sims/1-argon',\n",
    "    num_steps = 1e3, gpu=1\n",
    ")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import MDAnalysis as mda\n",
    "import nglview as nv\n",
    "\n",
    "u = mda.Universe(\"sims/1-argon/3-bonds.psf\",\"sims/1-argon/3-bonds.pdb\", \"sims/1-argon/output/3-bonds.dcd\")\n",
    "w = nv.show_mdanalysis(u)\n",
    "w.clear_representations()\n",
    "w.add_ball_and_stick(\"index < 10\",radius=10)\n",
    "w"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Why are there so many long bonds?\n",
    "print(bond.range_) ## Distances are in Angstroms\n",
    "r = np.linspace(*bond.range_, 20)\n",
    "print(r)\n",
    "print(bond.potential(r)) ## Energies are kcal/mol\n",
    "\n",
    "## Outside of this range, the bond applies zero force"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ARBD is configured through plain text files\n",
    "Use your favorite editor to examine the files named `sims/1-argon/*.bd`.\n",
    "ARBD uses *key-value* pairs to configure most features. The configuration parser does not use an interpretter, so math and string operations may not be used inside .bd files. For a list of the available keywords, see the documentation that comes with ARBD. \n",
    "\n",
    "Also, please note that ARBD is under development; some keywords have been deprecated, and the syntax is subject to evolve. The user-oriented interface of the arbdmodel package is less likely to change, which is one of the reasons we strongly recommend using arbdmodel to set up your simulation systems. Nevertheless, if you want to use ARBD, it's a good idea to familiarize yourself with the configuration file syntax."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
